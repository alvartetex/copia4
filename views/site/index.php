<?php

/** @var yii\web\View $this */

use yii\helpers\Html;
use coderius\swiperslider\SwiperSlider;

$this->title = 'Banesto Biking';
?>

<!-- Slider de indurain + titulo-->
<div class="site-index">
    
    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4 coloresletrablanca">Hoy hablamos de de Miguel Indurain</h1>
        <div class="row">
        <div class="container col-md-6">
            
        <?= SwiperSlider::widget([
            'slides' => [
            Html::img('@web/images/indurain/indurain1.jpg', ['alt' => 'Imagen no encontrada', 'class' => 'imagen']),
            Html::img('@web/images/indurain/indurain2.jpg', ['alt' => 'Imagen no encontrada', 'class' => 'imagen']),
            Html::img('@web/images/indurain/indurain3.jpg', ['alt' => 'Imagen no encontrada', 'class' => 'imagen']),
            Html::img('@web/images/indurain/indurain4.jpg', ['alt' => 'Imagen no encontrada', 'class' => 'imagen']),
                ],
            'options' => [
                'styles' => [
                    SwiperSlider::CONTAINER => ["height" => "350px", "width" => "75%"],
                    SwiperSlider::SLIDE => ["text-align" => "center"],
                    ],
                ],
            ]);

        ?>
    </div>
            <!-- Texto a la derecha de slider de indurain-->
        <div class=" col-md-6 coloresletrablanca" style=" text-align: left">
            <p> "El cuerpo aguanta más que la mente" cita textual del ciclista español considerado uno de los mejores del mundo. Hablamos de la leyenda del deporte Miguel Induráin, de 57 años de edad, que fue ganador de cinco Tours de Francia (1991-1995) y del Giro de Italia por dos años consecutivos (1992 y 1993), además de Campeón del mundo contrarreloj (1995) y poseedor del récord de la hora durante dos meses (1994).

Además de sus múltiples victorias en el Tour de Francia, Miguel Induráin ganó varias vueltas por etapas de una semana y clásicas de un día, destacando más la Volta a Cataluña, la París-Niza, la Clásica de San Sebastián, el Campeonato de España en Ruta y la Dauphiné Libére. ¡Sigue leyendo y descubre más sobre la vida y biografía de Miguel Induráin, una leyenda eterna del ciclismo español! </p>
        </div>
        </div>
   
     <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4 coloresletrablanca">Ultimas noticias</h1>
        <p></p>
        
       
    <!-- Slider de noticias -->
    <div class="row">
        
            
        <?= SwiperSlider::widget([
            'slides' => [
            Html::img('@web/images/noticias/noticia 3.PNG', ['alt' => 'Imagen no encontrada', 'class' => 'imagen']),
            Html::img('@web/images/noticias/noticia2.PNG', ['alt' => 'Imagen no encontrada', 'class' => 'imagen']),
            Html::img('@web/images/noticias/noticia333.png', ['alt' => 'Imagen no encontrada', 'class' => 'imagen']),
                ],
            'options' => [
                'styles' => [
                    SwiperSlider::CONTAINER => ["height" => "350px", "width" => "75%"],
                    SwiperSlider::SLIDE => ["text-align" => "center"],
                    ],
                ],
            ]);

        ?>
            </div>
        </div>
    </div> 
</div>
