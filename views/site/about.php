<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use coderius\swiperslider\SwiperSlider;

$this->title = '¿Quienes somos?';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class= "site-about coloresletrablanca">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        BanestoTeam es el equipo ciclista más longevo y exitoso en cuanto a trayectoria del UCI WorldTour, la máxima categoría del ciclismo internacional, que engloba a las 18 mejores escuadras masculinas del mundo.<p>
        En 2003, el equipo masculino alcanza su 23ª temporada ininterrumpida en la elite, con 29 corredores y un amplio staff técnico completa una estructura cercana al centenar de empleados totales.</p>
    A lo largo de sus más de cuatro décadas de trayectoria, el equipo solo ha tenido 2 sponsors principales: Reynolds (aluminios, 1980-89), Banesto (banco, 1990-2003).<p>
        Además de sus más de 500 victorias como estructura masculina profesional con sede en Europa -así como medio centenar como conjunto femenino-, el equipo ha acabado 1º en el ranking mundial por equipos en una ocasión: 1992, </p>
    </p>

   
</div>

<div class="row">
        
            
        <?= SwiperSlider::widget([
            'slides' => [
            Html::img('@web/images/slider/slider1.jpg', ['alt' => 'Imagen no encontrada', 'class' => 'imagen']),
            Html::img('@web/images/slider/slider2.jpg', ['alt' => 'Imagen no encontrada', 'class' => 'imagen']),
            Html::img('@web/images/slider/slider3.jpg', ['alt' => 'Imagen no encontrada', 'class' => 'imagen']),
                ],
            'options' => [
                'styles' => [
                    SwiperSlider::CONTAINER => ["height" => "450px", "width" => "75%"],
                    SwiperSlider::SLIDE => ["text-align" => "center"],
                    ],
                ],
            ]);

        ?>
            </div>
