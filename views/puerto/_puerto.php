<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
$clave = $model->nompuerto;
?>

<div class="card centrar separator zoom">
    <div class="card-body text-white bg-banesto">                        

        <div class="caption">
            <h2 class="titulo"><?= $model->nompuerto ?></h2>
            <p>Etapa: <?= $model->numetapa ?> - Categoría: <?= $model->categoria ?></p>
            <?php echo Html::img('@web/images/puertos/puerto'.$clave.'.jpg', [
                'alt' => 'Imagen no encontrada', 'class' => 'imagen'
                ]) ?>
            <hr class="my4">
            <h4>Ganador: <br><?= $model->nombre?></h4>
            <!-- Mostrar contador de etapas ganadas -->
            <p>Altura: <?= $model->altura?> - Pendiente: <?= $model->pendiente?></p>
        </div>

    </div>
</div> 

