<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
$clave = $model->numetapa;
?>

<div class="card centrar separator zoom">
    <div class="card-body text-white bg-banesto">                 

        <div class="caption">
            <h2>Etapa <?= $model->numetapa ?></h2>
            <p><?= $model->kms ?> KM</p>
            <?php echo Html::img('@web/images/etapas/etapa'.$clave.'.jpg', [
                'alt' => 'Imagen no encontrada', 'class' => 'imagen'
                ]) ?>
            <hr class="my4">
            <h4>Ganador: <br><?= $model->nombre?></h4>
            <!-- Mostrar contador de etapas ganadas -->
            <p><?= $model->salida?> - <?= $model->llegada?></p>
            <?php if ($model->puerts > 0): ?>
                <p><?= Html::a('Ver '.$model->puerts.' puertos',['/puerto/puertosganados','numtap' => $model->numetapa],['class'=>'btn btn-info'])?></p>
            <?php else: ?>
                <p class="btn btn-block" style="color: white">Sin Puertos</p>
            <?php endif; ?>
        </div>

    </div>
</div> 